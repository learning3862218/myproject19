﻿#include <iostream>

class Animal
{
public:
    virtual void Voice() = 0;
};

class Dog : public Animal
{
    void Voice() override
    {
        std::cout << "Wow-wow" << std::endl;
    }
};

class Cat : public Animal
{
    void Voice() override
    {
        std::cout << "Meow" << std::endl;
    }
};

class Mouse : public Animal
{
    void Voice() override
    {
        std::cout << "Pie-pie" << std::endl;
    }
};

int main()
{
    const int Size = 4;
    Animal* Animals[Size];
    for (int i = 0; i < Size; ++i)
    {
        Animals[i] = nullptr;
    }

    Animals[0] = new Dog;
    Animals[1] = new Cat;
    Animals[2] = new Mouse;
    Animals[3] = new Dog;

    for (int i = 0; i < Size; ++i)
    {
        if (Animals[i] != nullptr)
        {
            Animals[i]->Voice();
        }
    }

    for (int i = 0; i < Size; ++i)
    {
        delete Animals[i];
    }
}
